#! /usr/bin/env python
# -*- coding:utf-8 -*-
# Author: Arne Köhn <arne@arne-koehn.de>
# License GPLv3 or later
# usage: create_prefixes_virtual.py /path-to-out/%s-%i.cda /path/sentence1.cda ...
# %s is the original file name, %i the increment. %s has to be before %i.
# always remember to use absolute path names!

# from cda_parse import cdgoutparse
from cda_parse import cdgoutparse

CdaSpec = cdgoutparse.CdaSpec


import sys, os, copy, inspect


'''
Algorithm for generating prefixes with predictions
1. Determine needed words
   - all known words are needed
   - bottom up prediction: all words that are regents of needed words 
     are needed (recursivly)
   - words that are dependents of needed words 
     via a certain dependency type (label) are needed

2. Manage word ids
   - remove all unneded words
   - get the sequence of remaining word ids straight
   - update dependencies accordingly

3. write annotations
   - write known words, unchanged (besides possibly regent ids)
   - write virtual words 
     - with extra atteribute "virtual"
     - with generic name
     - with changed other attributes?


'''


predictables = ['SUBJ', 'OBJA', 'OBJD', 'PRED', 'PN', 'AVZ']
tagsToKeep   = ['cat', 'person', 'number', 'gender']

def getHead(word, lvl):
    return word.specs[lvl].pointer


def getLabel(word, lvl):
    return word.specs[lvl].value


def activate(s, i, needed):
    head = getHead(s[i], "SYN") -1
    if head >=0 and needed[head] == 0:
        needed[head] = 1
        activate(s, head, needed)



def pointerdec(s, i):
    '''increases all pointers in s that are greater than i'''    
    for w in s:
        if w.start > i:
            w.start -= 1
            w.end   -= 1
        for k,v in w.specs.iteritems():
            if v.type== 'dep':
                if v.pointer > i+1:
                    v.pointer -= 1 


def printsen(s):
    print "sentence:"
    for w in s:
        print "%s -> %s -> %d" % (w.word, getLabel(w,'SYN'), getHead(w,'SYN'))
        #for k,v in w.specs.iteritems():
        #    print "%s: (%s) %s -> %s -> %d" % (k,v.type, v.key, v.value, v.pointer)


def virtualize(s, vsid):
    ''' 
    check whether words with index > vsid are necessary 
    if no, they are removed (and ids changed accordingly)
    if yes, they are changed to generic virtual words
    '''

    #print "------------------------------------"
    #print "virtualizing from word " + str(vsid) 
    
    #printsen(s)

    # start with all words marked as not needed
    needed = map(lambda x: 0, s)

    #map(lambda x: sys.stdout.write(str(x)), needed)
    #print ""

    # now mark all words in the known prefix as needed
    for i in range(vsid):
        needed[i] = 1

    #map(lambda x: sys.stdout.write(str(x)), needed)
    #print ""

    # now mark all heads of needed words as needed
    for i in range(len(needed)):
        if needed[i] == 1:
            activate(s, i, needed) # works recursively

    #map(lambda x: sys.stdout.write(str(x)), needed)
    #print ""

    # now it's time for the dependents
    # a word with a needed head will be activated, depending on the label
    change = 1
    while change == 1:
        change = 0
        for i in range(len(needed)):
            if needed[i] == 0:
                head = getHead( s[i], "SYN") - 1
                label= getLabel(s[i], "SYN")
                if head >= 0 and needed[head] ==1 and label in predictables: # TODO deplabels 
                    change = 1
                    needed[i] = 1

    #map(lambda x: sys.stdout.write(str(x)), needed)
    #print ""

    # now remove unused words and clean up ids
    change=1
    while change ==1:
        change=0
        n = len(needed)
        for i in range(n):
            if needed[i] ==0:
                s.pop(i)
                needed.pop(i)
                pointerdec(s, i)
                change = 1
                break

    #printsen(s)

    for i in range(vsid,len(s)):
        w = s[i]
        #print "Word nr %d" % (vsid)
        # remove specifications
        newspecs= {}
        newspecs["virtual"]= CdaSpec("tag", "virtual", "true", 0)
        newspecs["base"]= CdaSpec("tag", "base", "unknown", 0)
        
        for k,v in w.specs.iteritems():
            #print "%s [%s]: %s (%d)" % (k, v.type, v.value, v.pointer)
            if v.type != 'tag':
                newspecs[k]= v
            elif k in tagsToKeep:
                newspecs[k]= v

            if k=='cat' and ( v.value=='VVFIN' or v.value=='VVINF' or v.value=='VVPP' or v.value=='VVIZU' ):
                newspecs['valence'] = CdaSpec("tag", "valence", "acis?+d?", 0)

            if k=='cat' and ( v.value=='VMFIN' or v.value=='VMINF' ):
                newspecs['valence'] = CdaSpec("tag", "valence", "ax", 0)            

            if k=='cat' and v.value== 'VAFIN':
                newspecs['valence'] = CdaSpec("tag", "valence", "egx", 0)
                newspecs['avz']     = CdaSpec("tag", "avz", "allowed", 0)
                newspecs['extravalence'] = CdaSpec("tag", "extravalence", "daci", 0)
                try:
                    newspecs['base']= w.specs['base']
                except:
                    pass

        w.specs= newspecs
        w.word = "[virtual]"
        

def write_prefixes(s0, fnametpl, sentencename):
    '''writes prefixes for given sentence s. fnametpl is a template
    for the output files (e.g. "/path/sentence-%i.cda"). pointers
    pointing to the future are set to -1'''

    #print "length: " + str(len(s0))

    for i in xrange(len(s0)):

        s = copy.deepcopy(s0)

        filename = fnametpl % (sentencename, i) 
        f = open(filename, 'w')
        sentenceid = os.path.basename(filename)[:-4]
        virtualize(s,i+1)
        f.write("'%s' : '%s' <->\n"%(sentenceid, sentenceid))
        print sentenceid + ":" + sentenceid 
        for j in range(len(s)):
            if j>0: # no comma before the first word
                f.write(",\n")
            w = s[j] # word
            f.write(" %s %s '%s'\n" % (w.start, w.end, w.word.replace("'",r"\'")))
            
            for k,v in w.specs.iteritems():
                if v.type == "tag":
                    f.write("'%s' / '%s'\n"%(str(v.key), str(v.value)))

            for k,v in w.specs.iteritems():
                if v.type != "tag":
                    pointer = v.pointer
                    f.write("'%s' -> '%s' -> %i\n"%(v.key, v.value, pointer))

        f.write(";\n")
        f.close()
    

outdirtmpl = sys.argv[1]
for i in sys.argv[2:]:
    f = open(i)
    sentence = cdgoutparse.parser.parse(f.read())
    sname = os.path.basename(i)[:-4] # strip the file ending (.cda)
    write_prefixes(sentence, outdirtmpl, sname)

