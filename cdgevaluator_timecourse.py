#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2010-2012  Niels Beuck
# Copyright (C) 2010-2013  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# NOTE: only minimal prediction will yield stability since it can't be
# easily computed with structural prediction.

from cda_parse.cdgoutparse import parser
import conllparser

import copy
import traceback
import logging
import sys
import argparse


argparser = argparse.ArgumentParser(description="Evaluates predictive analyses"
                                    " against a gold standard of predictive"
                                    " analyses")
argparser.add_argument("-v", "--verbosity", type=int,
                       dest="verbosity", default=1,
                       help="0=error, 1=warn(default), 2=info, 3=debug")
argparser.add_argument("-m", "--malt",
                       action="store_true", dest="malt", default=False,
                       help="expect malt style prefix files, like starting "
                       "with 1 and an extra final result")
argparser.add_argument("-r", "--margin",
                       action="store_true", dest="margin", default=False,
                       help="add extra prefixes in the beginning and the end"
                       " to assure the same number of counts for every word")
argparser.add_argument("-s", "--structural",
                       action="store_true", dest="structural", default=False,
                       help="evaluate structural prediction,"
                       " i.e. virtual nodes")
argparser.add_argument("-n", "--splitnonspec",
                       action="store_true", dest="splitnonspec", default=False,
                       help="handle nonspec as several virtual nodes,"
                       " instead of one")
                        #both is wrong but we have to decide in structural mode
argparser.add_argument("-u", "--unusednode",
                       action="store_true", dest="unused", default=False,
                       help="assume that the last node serves as a node to"
                       " attach unused virtual nodes to.")
argparser.add_argument("testglob", help="path to results, with sentence id"
                       " replaced by %%id and increment number by %%incr")
argparser.add_argument("goldglob", help="path to gold standard, with sentence"
                       " id replaced by %%id and increment number by %%incr")
argparser.add_argument("start_id", help="id of the first sentence to evaluate",
                       type=int)
argparser.add_argument("stop_id", help="id of the last sentence to evaluate",
                       type=int)
argparser.add_argument("window_left", help="how many slots the window should"
                       " extend to the left", type=int)
argparser.add_argument("lookahead", help="how much lookahead the parser used",
                       type=int, nargs='?', default=0)

args = argparser.parse_args()
malt = args.malt
margin = args.margin
structural = args.structural
splitnonspec = args.splitnonspec

tPattern = args.testglob
gPattern = args.goldglob
minId = args.start_id
maxId = args.stop_id
nback = args.window_left
la = args.lookahead

logging.basicConfig()
logger = logging.getLogger(__name__)
verbosityMap = {0: logging.ERROR,
                1: logging.WARN,
                2: logging.INFO,
                3: logging.DEBUG}
try:
    logger.setLevel(verbosityMap.get(args.verbosity))
except:
    logger.error("Can't set log level to " + str(args.verbosity))
    exit(1)

if malt:
    logger.info("Malt mode")

# regent is / should be
# nv/nv nv/v v/nv v/v
errortypesOnFrontier = [0, 0, 0, 0]


def obsfunc1(prefix, gold, virtmap, results):
    # res[1] == correct unlabeled
    frontier = getLastNonVirt(prefix)-1
    if results[1][frontier] == 0:
#        sys.stderr.write("====================\noutput\n")
#        sys.stderr.write(toAnno(prefix))
#        sys.stderr.write("\nshoud be\n")
#        sys.stderr.write(toAnno(gold))
        if prefix[frontier].specs["SYN"].pointer > frontier+1:
            if gold[frontier].specs["SYN"].pointer > frontier+1:
                # v/v
                errortypesOnFrontier[3] += 1
            else:
                # v/nv
                errortypesOnFrontier[2] += 1
        else:
            if gold[frontier].specs["SYN"].pointer > frontier+1:
                # nv/v
                errortypesOnFrontier[1] += 1
            else:
                # nv/nv
                errortypesOnFrontier[0] += 1


def obsfunc_noop(*args):
    pass


obsfunc = obsfunc1


def better(score1, score2):
    """compares two scores for VN mapping, first by unlabeled and second
    by labeled matches"""
    if score1[0] < score2[0] \
       or (score1[0] == score2[0] and score1[1] < score2[1]):
        return True
    return False


def prepareNonspec(test):
    """adjusts attachments when in malt mode and splits nonspec into
    several time points if splitnonspec"""
    ns_id = -1

    for n in range(len(test)):
        tword = test[n]
        tp = tword.specs["SYN"].pointer
        tv = tword.specs["SYN"].value

        if malt:
            # in malt output nonspec attachments appear as nil
            # attachments with the 'null' label we have to change the
            # attachments accordingly
            if tv == 'null' and tp == 0:
                if tword.specs["cat"].value == '$.' \
                   or tword.specs["cat"].value == '$,' \
                   or tword.specs["cat"].value == '$.(':
                    tword.specs["SYN"].value = ''
                else:
                    tword.specs["SYN"].pointer = ns_id
                    if splitnonspec:
                        ns_id -= 1
        elif splitnonspec:
            # we still need to change -1 counters
            if tp == -1:
                tword.specs["SYN"].pointer = ns_id
                ns_id -= 1


# TODO

# - anderes la und margin handling verwerfen
# - dafür sorgen, dass das aktuelle wort richtig gesetzt ist

def preparePrefixList(truePrefixes, final, nback, la):
    '''makes a list of prefixes that inlcues margin and la handling'''
    result = []

    # add empty prefixes in the beginning
    # needed for margin handling and for lookahead
    for i in range(la):
        result.append([])

    # now add the regular prefixes, but ommit the last ones, depending
    # on the lookahead
    for i in range(len(truePrefixes) - la):
        result.append(truePrefixes[i])

    # add the final result once
    result.append(final)

    # if there is a margin handling we need to fill up some more with
    if margin:
        for i in range(nback):
            result.append(final)

    return result


def toAnno(anno):
    '''create string with all dependency edges in the annotation'''
    result = ""
    i = 1
    for word in anno:
        result += str(i) + " " + word.word + "--" \
                         + str(word.specs["SYN"].value) + "->" \
                         + str(word.specs["SYN"].pointer) + "\n"
        i += 1
    return result


def evalVM(vm, test, gold):
    '''evaluates the test annoation by comparison to the gold annotation
    virtual words are mapped by vm'''

    #print "==============="
    #print "called: evalVM"
    #print "vm: ", vm
    errors = [0, 0]
    for n in range(len(test) - args.unused):
        tword = test[n]
        wnum = n+1  # off by one between array indices and cdg counting scheme
        tp = tword.specs["SYN"].pointer
        tv = tword.specs["SYN"].value

        if wnum in list(vm.keys()):
            # dependent virtual

            if vm[wnum] == -1:
                # cannot be mapped
                errors[0] += 1
                errors[1] += 1
                continue

            else:
                # wnum instead of n, as we first have to look it up,
                gword = gold[vm[wnum]-1]
                                        # then subtract 1

        elif "virtual" in tword.specs:
            # virtual but not in virtmap, thus unused, skip it
            continue
        else:
            # print "n", n
            gword = gold[n]

        gp = gword.specs["SYN"].pointer
        gv = gword.specs["SYN"].value
        if tp in list(vm.keys()):
            # regent virtual

            if vm[tp] == -1:
                # cannot be mapped
                errors[0] += 1
                errors[1] += 1
                continue

            else:
                # can be mapped, map it
                tp = vm[tp]

        # independent of whether it has been mapped or not, compare it
        if gp == tp:
            if not gv == tv:
                # label is wrong
                errors[0] += 1
            # else: all is well
        else:
            # attachment is wrong
            errors[0] += 1
            errors[1] += 1

    # end for n in xrange...

    return errors
# end def evalVM


def bestVirtMap(test, gold):
    '''returns a mapping from virtual words in test to out-of-prefix words
    in gold the best possible mapping wrt attachment score is
    returned
    '''

    # first get the index of the last non-virtual node
    lastnonvirt = 0  # in cdg space, where 0 = nil, not array space
    for i in range(len(test) - args.unused):
        if 'virtual' in test[i].specs:
            break
        lastnonvirt += 1
    # lastnonvirt -= 1  # not needed, off by one error compensates
    logger.debug("------------------------")
    logger.debug("lastnonvirt: "+str(lastnonvirt))

    # calculate optimal mapping from virtual nodes to upcoming words
    virtmap = {}
    candidates = {}

    # collect candidates
    # virtual node as regent
    for n in range(lastnonvirt):
        tword = test[n]
        if n >= len(gold):
            logger.warning("word "+tword.word+" not in annotation")
        gword = gold[n]
        gp = gword.specs["SYN"].pointer
        wnum = n+1
        tp = tword.specs["SYN"].pointer 
        if tp > lastnonvirt or tp <= -1 :
            if not tp in list(candidates.keys()):
                candidates[tp]= [-1]
            if gp> lastnonvirt :
                candidates[tp].append(gp)

    # virtual node as dependent
    for n in range(lastnonvirt,len(test) - args.unused):
        tword= test[n]
        wnum= n+1
        tp = tword.specs["SYN"].pointer
        if tp <= lastnonvirt and not (tp == 0 and tword.specs["SYN"].value == ""):
            #print wnum, "--", tword.specs["SYN"].value,"-> ", tp 
            # check possible dependents
            for m in range(lastnonvirt, len(gold)):
                gword= gold[m]
                wnum2= m+1
                if gword.specs["SYN"].pointer == tword.specs["SYN"].pointer:
                    if not wnum in list(candidates.keys()):
                        candidates[wnum]= [-1]
                    if wnum2> lastnonvirt :
                        candidates[wnum].append(wnum2)

    # handle purely virtual dependents
    for n in range(lastnonvirt,len(test) - args.unused):
        tword= test[n]
        wnum= n+1
        tp= tword.specs["SYN"].pointer
        # tp is the index of a virtual node (and not of "unused") or
        # it's a nonspec pointer
        if (tp > lastnonvirt and tp < len(test) - 1 - args.unused) or tp <= -1:
            if not tp in list(candidates.keys()):
                candidates[tp] = [-1]
            if not wnum in list(candidates.keys()):
                candidates[wnum] = [-1]
                # no use to go on, as there are no candidates, continue
                continue

            # add new candidates for the regent
            for cand in candidates[wnum]:
                if cand != -1:
                    cword = gold[cand-1]
                    cp = cword.specs["SYN"].pointer
                    if cp > lastnonvirt and not cp in candidates[tp]:
                        candidates[tp].append(cp)

            # for all candidates of its regent
            for cand in candidates[tp]:
                for m in range(lastnonvirt, len(gold)):
                    gword = gold[m]
                    wnum2 = m+1
                    if gword.specs["SYN"].pointer == cand:
                        if not wnum in list(candidates.keys()):
                            candidates[wnum] = [-1]
                        if wnum2 > lastnonvirt:
                            candidates[wnum].append(wnum2)

    logger.debug("candidates: " + str(candidates))

    # now consolidate the candidates to one interpretation

    # first simply make sets
    for key in list(candidates.keys()):
        candidates[key] = list(set(candidates[key]))

    # we need to know if there are contested target nodes
    targets = {}
    for key in list(candidates.keys()):
        for cand in candidates[key]:
            if not cand in list(targets.keys()):
                targets[cand] = []
            targets[cand].append(key)

    for key in list(targets.keys()):
        targets[key] = list(set(targets[key]))

    # assign all unique matchings
    remaining = {}
    for key in list(candidates.keys()):
        if len(candidates[key]) == 1 and len(targets[candidates[key][0]]) == 1:
            virtmap[key] = candidates[key][0]
        else:
            remaining[key] = candidates[key]

    # now let the fun part begin: try out all combinations and keep the best
    vmcs = [[copy.copy(virtmap), []]]
    for key in list(remaining.keys()):
        new_vmcs = []
        for vmc in vmcs:
            for cand in remaining[key]:
                if not(cand in vmc[1]):
                    new_vmc = copy.copy(vmc[0])
                    new_vmc[key] = cand
                    new_vmc1 = copy.copy(vmc[1])
                    new_vmc1.append(cand)
                    new_vmcs.append([new_vmc, new_vmc1])
            new_vmc = copy.copy(vmc[0])
            new_vmc[key] = -1  # no mapping
            new_vmcs.append([new_vmc, copy.copy(vmc[1])])
        vmcs = new_vmcs

    # search for the best one
    best = [1000, 1000]  # [labelled, unlabelled]
    best_vm = {}
    if(len(vmcs) == 0):
        logger.error("no mapping found!")
        # TODO: What happens after this error?
    for vmcl in vmcs:
        vmc = vmcl[0]
        score = evalVM(vmc, test, gold)
        if better(score, best):
            best_vm = vmc
            best = score

    virtmap = best_vm
    logger.debug("selected mapping: " + str(virtmap))

    return virtmap


def countRoots(test):
    '''used to measure the connectedness
    expect there to be no cycles
    simply count the words attached to nil or nonspec '''

    roots = 0
    for i in range(len(test) - args.unused):
        tword = test[i]
        tp = tword.specs["SYN"].pointer
        tv = tword.specs["SYN"].value
        if tp < 0 or (tp == 0 and not tv == "") \
           or (not structural and i>getLastNonVirt(test)-1 and not tv == ""):
            roots += 1
    if roots > 0:
        return roots - 1
    else:
        return 0


def getLastNonVirt(prefix):
    '''returns an index in cdg space, where 0 = nil, not array space'''
    lastnonvirt = 0
    for i in range(len(prefix)):
        if 'virtual' in prefix[i].specs or prefix[i].word == "__NONSPEC__":
            break
        lastnonvirt += 1
    return lastnonvirt


def evalDependencies(test, gold, virtmap):
    '''evaluate all dependencies present in test b comparing them to the
    dependencies in gold returns for every word in gold whether it was
    included at all and wether it was attached correctly there are
    three arrays, called unspecified, correct-unlabeled,
    correct-labled the return value [[0,0,1],[1,0,0],[1,0,0]] would
    indicate the first word was attached correctly (both labeled and
    unlabeled), the attachment for the second word was wrong and the
    third word was not included at all (TODO there are more elements
    than that in the ouput now)'''

    logger.debug("========================")
    logger.debug(" test:")
    logger.debug(toAnno(test))
    logger.debug("------------------------")
    logger.debug("gold:")
    logger.debug(toAnno(gold))

    # first get the index of the last non-virtual node
    lastnonvirt = getLastNonVirt(test)

    unspec  = [1]*len(gold)
    cor_l   = [0]*len(gold)
    cor_u   = [0]*len(gold)
    nspec   = [0]*len(gold)
    ns_c_l  = [0]*len(gold)
    ns_c_u  = [0]*len(gold)
    strPred = [0]*4  # (count , cor_u, cor_l, no_map)

    # --------------------------------------
    # calculate accuracy scores
    # by iterating through all tokens
    # in test
    # first the nonvirtual ones
    for n in range(lastnonvirt):
        tword = test[n]
        tp = tword.specs["SYN"].pointer
        tv = tword.specs["SYN"].value
        gword = gold[n]
        wnum = n+1

        try:
            unspec[n] = 0

            # regent = nonspec
            if tp <= -1:
                if structural:
                    #handle nonspec as one single virtual node
                    if virtmap[tp] == gword.specs["SYN"].pointer:
                        cor_u[n] = 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] = 1

                else:
                    # handle nonspec as generic future node
                    nspec[n] += 1
                    if gword.specs["SYN"].pointer > lastnonvirt \
                       or gword.specs["SYN"].pointer == -1:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
            # regent = virtual node
            elif tp > lastnonvirt:
                # handle as generic future node, like nonspec
                if structural:
                # handle as specific virtual node
                    logger.debug("-------------------------------------")
                    logger.debug("known to virtual node")
                    logger.debug("test  :" + str(wnum) + " --"
                                 + tv + "-->" + str(tp))
                    logger.debug("mapped:" + str(wnum) + " --"
                                 + tv + "-->" + str(virtmap[tp]))
                    logger.debug("gold :" + str(wnum) + " --"
                                 + gword.specs["SYN"].value + "-->"
                                 + str(gword.specs["SYN"].pointer))
                    nspec[n] += 1
                    if virtmap[tp] == gword.specs["SYN"].pointer:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
                else:
                    nspec[n] += 1
                    if gword.specs["SYN"].pointer > lastnonvirt:
                        cor_u[n] += 1
                        ns_c_u[n] += 1
                        if tv == gword.specs["SYN"].value:
                            cor_l[n] += 1
                            ns_c_l[n] += 1
            # regent known
            else:
                if tp == gword.specs["SYN"].pointer:
                    cor_u[n] += 1
                    if tv == gword.specs["SYN"].value:
                        cor_l[n] += 1
        except:
            logger.error("!!!!!!!!!!!!error!!!11!")
            continue  # something is wrong with this entry, just ignore it
    # end for  wnum in xrange(lastnonvirt+1)

    if structural:
        #now iterate over virtual nodes, if any
        for n in range(lastnonvirt, len(test) - args.unused):
            tword = test[n]
            wnum = n+1
            tp = tword.specs["SYN"].pointer
            tv = tword.specs["SYN"].value

            # skip unused nodes
            if (tp == 0 and tv == "") or (args.unused and tp == len(test)):
                continue

            # treat this edge as a virtual one, no matter the regent
            # count structurally predicted nodes seperately
            strPred[0] += 1
            try:
                if(wnum in list(virtmap.keys())) and virtmap[wnum] != -1:
                    gword = gold[virtmap[wnum]-1]
                    gp = gword.specs["SYN"].pointer
                    gv = gword.specs["SYN"].value
                    unspec[virtmap[wnum]-1] = 0
                else:
                    # there is no mapping for this node
                    strPred[3] += 1
                    continue  # counted in cnt, but not as correct occurence

                # regent == nonspec
                if tp <= -1:
                    # handle nonspec as one single virtual node
                    nspec[virtmap[wnum]-1] += 1
                    if virtmap[tp] == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1] += 1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2] += 1

                # regent = also a virtual node
                elif tp > lastnonvirt:
                    # FIXME this code breaks!
                    # logger.debug("-------------------------------------")
                    # logger.debug("virtual to virtual node")
                    # logger.debug("test : " + str(wnum) + "--" +
                    #              str(tv) + "-->" + str(tp))
                    # logger.debug("mapped: "
                    #              + str(virtmap[wnum]) + "--" + str(tv)
                    #              + "-->" + str(virtmap[tp]))
                    # logger.debug("gold : "
                    #              + str(virtmap[wnum]) + "--" + str(gv)
                    #              + "-->" + str(gp))

                    # handle as specific virtual node
                    nspec[virtmap[wnum]-1] += 1
                    if tp in list(virtmap.keys()) and virtmap[tp] == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1] += 1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2] += 1
                # regent known
                else:
                    logger.debug("-------------------------------------")
                    logger.debug("virtual to known node")
                    logger.debug("test : " + str(wnum) + "--" +
                                 str(tv) + "-->" + str(tp))
                    logger.debug("mapped: " + str(virtmap[wnum]) +
                                 "--" + str(tv) + "-->" + str(tp))
                    logger.debug("gold : " + str(virtmap[wnum]) +
                                 "--" + str(gv) + "-->" + str(gp))

                    nspec[virtmap[wnum]-1] += 1
                    if tp == gp:
                        cor_u[virtmap[wnum]-1] += 1
                        ns_c_u[virtmap[wnum]-1] += 1
                        strPred[1] += 1
                        if tv == gv:
                            cor_l[virtmap[wnum]-1] += 1
                            ns_c_l[virtmap[wnum]-1] += 1
                            strPred[2] += 1
            except:
                logger.error(str(traceback.format_exc()))
                logger.error("something wrong with word" + tword.word
                             + ", skipping...\n")
                continue  # something is wrong with this entry, just ignore it
        # end for
    # end if structural

    return [unspec, cor_u, cor_l, nspec, ns_c_u, ns_c_l, strPred]
# end def evalDependencies


def evalSentence(allPrefixes, gold_prefixes, nback):
    '''evaluates all prefixe parses for a sentence and returns a
    comprehensive analysis returns an array [total, unspec, correct_u,
    correct_l, final]
    '''

    total     = [0]*( 1 + nback )
    unspec    = [0]*( 1 + nback )
    correct_u = [0]*( 1 + nback )
    correct_l = [0]*( 1 + nback )
    stable_u  = [0]*( 1 + nback )
    stable_l  = [0]*( 1 + nback )
    nspec     = [0]*( 1 + nback )
    ns_c_u    = [0]*( 1 + nback )
    ns_c_l    = [0]*( 1 + nback )
    
    #structural Prediction
    strPred = [0]*4 # (count, cor_u, cor_l, no_map)

    n = len(gold_prefixes)

    if malt:
        if n != len(allPrefixes)-1:
            logger.warning("test and gold not of equal length: " +
                           str(len(allPrefixes)-1) + " vs " + str(n))
    else:
        if n != len(allPrefixes):
            logger.warning("WARNING: test and gold not of equal length: " +
                           str(len(allPrefixes)-1) + " vs " + str(n))

    # for margin and lookahead handling as well as malt specific
    # requirements, we have to manipulate the list of prefixes
    if malt:
        prefixes = preparePrefixList(allPrefixes[:-2],
                                     allPrefixes[-1], nback, la)
    else:
        prefixes = preparePrefixList(allPrefixes[:-1],
                                     allPrefixes[-1], nback, la)
    i = 0
    results = []
    goldRoots = countRoots(gold_prefixes[-1])
    # FIXME rootberechnung auf Basis der gold-inkremente?
    roots = 0
    rootsDistri = [0]*11

    # FIXME
    num_virt_gold = 0

    for inc_num in range(len(prefixes)):
        prefix = prefixes[inc_num]
        try:
            gold = gold_prefixes[inc_num]
        except:
            # FIXME this is a hack to circumvent "overshooting" when
            # using lookahead
            gold = gold_prefixes[-1]

        num_virt_gold += len(gold) - (inc_num + 1)

        prepareNonspec(prefix)
        if structural:
            virtmap = bestVirtMap(prefix, gold)
        else:
            virtmap = None
        results = evalDependencies(prefix, gold, virtmap)
        obsfunc(prefix, gold, virtmap, results)
        if not structural:
            stability = evalDependencies(prefix, allPrefixes[-1], virtmap)
        r = countRoots(prefix)
        roots += r
        if r >= len(rootsDistri):
            r = len(rootsDistri)-1
        rootsDistri[r] += 1

        for j in range(-nback, 1):
            k = i + j
            #k= i + j +la # + la, because, with lookahead
                          # the input is actually further along than i
            #if i+la >= n: # don't delay the words at the right
                           # end of the sentence
            #    k-= i+la - (n-1)
            if k >= 0 and k < n:
                total[j+nback] += 1
                unspec[j+nback] += results[0][k]
                correct_u[j+nback] += results[1][k]
                correct_l[j+nback] += results[2][k]
                if not structural:
                    stable_u[j+nback] += stability[1][k]
                    stable_l[j+nback] += stability[2][k]
                nspec[j+nback] += results[3][k]
                ns_c_u[j+nback] += results[4][k]
                ns_c_l[j+nback] += results[5][k]

        for j in range(4):
            strPred[j] += results[6][j]
        i += 1
    # end for all prefixes

    strPred.append(num_virt_gold)

    final_t = len(gold)
    final_c_u = sum(results[1])
    final_c_l = sum(results[2])
    final_ns = sum(results[3])
    final = [final_t, final_c_u, final_c_l, final_ns]
    root = [goldRoots, len(prefixes), roots, rootsDistri]
    return [total, unspec, correct_u, correct_l, stable_u, stable_l,
            nspec, ns_c_u, ns_c_l, final, root, strPred]


def readPrefixes(tPattern, id, start_prefix):
    '''reads all prefixe parses of a sentence
    returns a list of parses'''
    test = []
    conll_mode = tPattern.endswith("conll")
    i = start_prefix
    while True:
        try:
            flo = open(tPattern.replace("%id", str(id)).replace("%incr", str(i)))
            content = str(flo.read())
            if (conll_mode):
                parsed = conllparser.parse(content)
            else:
                parsed = parser.parse(content)
            test.append(parsed)
            i += 1
        except:  # Exception as e:
            #sys.stderr.write(str(traceback.format_exc()))
            #sys.stderr.write(str(e))
            break
    return test


def evaluateAll(tPattern, gPattern, minId, maxId, nback):

    results = []
    d = 1 + nback

    # 0-8, total unspec cor_u cor_l stab_u stab_l nonspec ns_c_u ns_c_l
    for i in range(9):
        results.append([0]*d)
    results.append([0]*4)  # 9, final result
    results.append([0]*3)  # 10, connectedness
    results.append([0]*11)  # 11, root/fragment counting
    results.append([0]*5)  # 12, structural predictions

    for id in range(minId, maxId+1):
        logger.info("reading sentence "+str(id))

        try:
            gold = readPrefixes(gPattern, id, 1)
        except:
            logger.warning("could not read gold file, skipping...")
            continue

        #print "len: %d"%(len(gold))
        try:
            test = readPrefixes(tPattern, id, 1)
        except:
            logger.warning("could not read result file, skipping...")
            continue

        try: 
            rs = evalSentence(test, gold, nback)
        except Exception as e:
            logger.warning(str(traceback.format_exc()))
            logger.warning(str(e))
            logger.warning("WARNING: files out of sync")
            continue

        # read results
        for i in range(9):
            for j in range(d):
                results[i][j] += rs[i][j]

        # final results
        for j in range(4):
            results[9][j] += rs[9][j]

        # connectedness
        for j in range(3):
            results[10][j] += rs[10][j]
        for j in range(len(results[11])):
            results[11][j] += rs[10][3][j]

        # structural predictions
        for j in range(5):
            results[12][j] += rs[11][j]

    return results


results = evaluateAll(tPattern, gPattern, minId, maxId, nback)


print("# error types on frontier: regent is / should be nv/nv nv/v v/nv v/v")
print("# " + str(errortypesOnFrontier))
print("# connectedness:")
print("# gold: %f" % (results[10][0]/float(maxId-minId+1)))
print("# test: %f" % (results[10][2]/float(results[10][1])))
s = "distri: "
for i in range(len(results[11])):
    s += "%d," % (results[11][i])
print("#", s)
print("# num virtual nodes in gold standard: ", results[12][4])
print("#")


print("# id total unspec cor_u cor_l stab_u stab_l nonspec ns_c_u ns_c_l")
print("pred-prec %d 0 %d %d 0 0 %d %d %d %d %d %d %d"%(results[12][0], results[12][1], results[12][2], results[12][0], results[12][1], results[12][2],results[12][0], results[12][1], results[12][2], results[12][3]))
# empty line to create horizontal space in gnuplot
print("  0 0 0 0 0 0 0 0 0 0 0 0 0")
for i in range(nback+1):
    ii = -i + nback
    print("%d %d %d %d %d %d %d %d %d %d 0 0 0 0" % (i, results[0][ii], results[1][ii], results[2][ii], 
                                             results[3][ii], results[4][ii], results[5][ii],
                                             results[6][ii], results[7][ii], results[8][ii]))
# empty line to create horizontal space in gnuplot
print("  0 0 0 0 0 0 0 0 0 0 0 0 0")
print("%s %d 0 %d %d %d %d %d 0 0 0 0 0 0" % ('final', results[9][0], results[9][1], 
                             results[9][2], results[9][0], results[9][0], results[9][3]))
