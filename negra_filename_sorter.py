#! /usr/bin/env python

import sys,re

sortre = re.compile(r".*negra-s(\d+)-(\d+)")

def negracmp(x,y):
    x1,x2 = sortre.match(x).groups()
    y1,y2 = sortre.match(y).groups()
    res = cmp(int(x1),int(y1))
    if res != 0:
        return res
    return cmp(int(x2),int(y2))

negras = sys.stdin.readlines()
delimiter = '' # if one-file-one line -> \n still at each file ending
if len(negras) == 1:
    # assume that files are separated by whitespace
    negras = negras[0].split()
    delimiter = ' '
negras.sort(cmp=negracmp)
print delimiter.join(negras)
