#! /usr/bin/env python
# -*- coding:utf-8 -*-

# Authors: Niels Beuck <beuck@informatik.uni-hamburg.de>
#          Arne Köhn <arne@arne-koehn.de>

from cda_parse import cdgoutparse

CdaSpec = cdgoutparse.CdaSpec
CdaWord = cdgoutparse.CdaWord


#############################
# shortcuts for data access #
#############################

def isVirtual(word, vsid):
    return word.start >= vsid


def isVirtualByFeat(word):
    return checkFeat(word, "virtual", "true")


def isNeeded(word):
    return hasFeat(word, 'needed') and getFeat(word, 'needed') == 'true'


def setNeeded(word, b):
    if b:
        word.specs['needed'] = CdaSpec('tag', 'needed', 'true', 0)
    else:
        word.specs['needed'] = CdaSpec('tag', 'needed', 'false', 0)


def removeNeededMarker(word):
    if 'needed' in word.specs:
        del word.specs['needed']


def isMarked(word):
    return hasFeat(word, 'marked') and getFeat(word, 'marked') == 'true'


def setMarked(word, b):
    if b:
        word.specs['marked'] = CdaSpec('tag', 'marked', 'true', 0)
    else:
        word.specs['needed'] = CdaSpec('tag', 'marked', 'false', 0)


def removeMarker(word):
    if 'marked' in word.specs:
        del word.specs['marked']


def hasFeat(word, feat):
    return feat in word.specs


def getFeat(word, feat):
    return word.specs[feat].value


def getHead(word, lvl):
    return word.specs[lvl].pointer


def getLabel(word, lvl):
    return word.specs[lvl].value


#################
# navigate tree #
#################
def getHeadOf(word, sentence, lvl):
    h = getHead(word, lvl)-1
    if h >= 0:
        return sentence[h]
    else:
        return None


def getChildrenOf(word, sentence, lvl):
    children = []
    for w in sentence:
        if word.end == getHead(w, lvl):
            children += [w]
    return children


###################
# transform words #
###################
def setLabel(word, lvl, newLabel):
    word.specs[lvl].value = newLabel


###################
# check attribute #
###################
def checkLabel(word, lvl, label):
    return word.specs[lvl].value == label


def checkFeat(word, feat, val):
    if not(feat in word.specs):
        return False
    return word.specs[feat].value == val


###################
# transform tree  #
###################

def setHead(word, lvl, head):
    word.specs[lvl].pointer = head


def setFeat(word, feat, val):
    word.specs[feat].value = val


def foldUp(word, sentence):
    '''
    remove the regent of the given word from the tree
    and change all its in- and outgoing dependencies to the given word,
    '''
    upPos = getHead(word, "SYN")
    upWord = getHeadOf(word, sentence, "SYN")

    # first change the regent of the given word to be the regent of
    # its former head
    setHead(word, "SYN", getHead(upWord, "SYN"))
    setLabel(word, "SYN", getLabel(upWord, "SYN"))

    # then change all attachments to the head to the given word
    for w in sentence:
        if getHead(w, "SYN") == upPos:
            setHead(w, "SYN", word.end)

    # finally deattach the word just folded away
    setHead(upWord, "SYN", 0)
    setLabel(upWord, "SYN", '')
    setNeeded(upWord, False)


def foldDown(word, sentence):
    '''
    remove the given word from the tree
    and change all its in- and outgoing dependencies to its head,
    '''
    upPos = getHead(word, "SYN")

    # change all attachments to the given word to point to its head instead
    for w in sentence:
        if getHead(w, "SYN") == word.end:
            setHead(w, "SYN", upPos)

    # finally deattach the word just folded away
    setHead(word, "SYN", 0)
    setLabel(word, "SYN", '')
    setNeeded(word, False)

#===========================================

def findNonVirtChildren(word, sentence, lvl):
    cs = getChildrenOf(word, sentence, lvl)
    ret = []
    for c in cs:
        if isVirtualByFeat(c):
            ret += findNonVirtChildren(c, sentence, lvl)
        else:
            ret += [c]
    return ret


def leftmostWord(l):
    ret = None
    pos = 1000
    for w in l:
        if w.start < pos:
            ret = w
            pos = w.start
    return ret


def findBuPredictor(w, sent):
    lvl = 'SYN'
    botUp = leftmostWord(findNonVirtChildren(w, sent, lvl))
    if botUp:
        return leftmostWord(getChildrenOf(w, sent, lvl))
    return None

#===========================================

## TODO
# DONE Regeln zum isneeded setzen
# DONE Not needed words rausschmeißen
# DONE Needed marker entfernen 
# DONE wrapper, der ales in der richtigen reihenfolge auffruft
# TODO testen testen testen

#printing


def printsen(s):
    print("sentence:")
    for w in s:
        print("%s -> %s -> %d" % (w.word, getLabel(w, 'SYN'),
                                  getHead(w, 'SYN')))


def printword(w):
    print("%s (%d-%d)" % (w.word, w.start, w.end))
    for k, v in w.specs.items():
        if v.type == 'tag':
            print("%s : %s" % (k, v.value))
        else:
            print("%s --%s--> %d" % (k, v.value, v.pointer))

'''
when to keep:
- all words in the prefix are kept
- heads of kept words are kept, recursively
- words filling valencies are kept
  - subjects of all verbs
  - objects of virtual verbs
  - certain other labels
- KON unter kept KON
- ADV mit cat2=KON ??? wie viele sind das?
- KON unter non-virt TRUNC (aber auch PPs...)

transformations:
- OBJP von virt nach PP
- PRED ja nach cat nach
  - OBJA für nominals
  - PP für preps
  - ADV adjective??

folding (wort entfernen, alle referenzen auf den regenten umbiegen)
- virt AUX unter virt
- virt KON unter virt KON

'''
