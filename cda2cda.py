#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# Authors: Arne Köhn <arne@chark.eu>
# License GPLv3 or later
#
# if you wonder why somebody might need this: You can change files
# manually and run them through this script to make them look good
# again.

from cda_parse import cdgoutparse

CdaSpec = cdgoutparse.CdaSpec
CdaWord = cdgoutparse.CdaWord

import os
import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-i', '--indir',
                        help = "Where to read cda files from")
    parser.add_argument('-o', '--outdir',
                        help = "Where to store cda files")
    return parser.parse_args()


def write_sentence(sentence_struct, id, outfile):
    f = open(outfile, "w")
    f.write("'%s' : '%s' <->\n"%(id, id))
    first = True
    for w in sentence_struct:
        if not first:
            f.write(",\n")
        first = False
        f.write(w.str_with_comment(sentence_struct))
    f.write(";\n")


def main():
    args = parse_arguments()
    indirlength = len(args.indir)
    for dirpath, dirnames, filenames in os.walk(args.indir):
        outdir = os.path.join(args.outdir, dirpath[indirlength:])
        os.makedirs(outdir, exist_ok=True)
        for filename in filenames:
            if filename.endswith(".cda"):
                infile = os.path.join(dirpath, filename)
                print(infile)
                outfile = os.path.join(outdir, filename)
                with open(infile) as f:
                    cda = cdgoutparse.parser.parse(f.read())
                    write_sentence(cda, filename[:-4], outfile)

if __name__ == '__main__':
    main()
