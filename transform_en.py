# -*- coding:utf-8 -*-
# Copyright (C) 2012-2013  Niels Beuck
# Copyright (C) 2013  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''rules for virtualizing English'''


import tree as t
from cda_parse.cdgoutparse import CdaSpec, CdaWord


def keepGAP(sentence, vsid, onlyOne):
    '''gapping constructs can either be predicted completely or only the
       rightmost one.'''
    for w in sentence[:vsid]:
        # find the rightmost GAP child of a COORD
        if t.getLabel(w, "SYN") == "COORD":
            for gap in t.getChildrenOf(w, sentence, "SYN"):
                if "GAP" in t.getLabel(gap, "SYN"):
                    t.setNeeded(gap, True)
                    break


def applyAllRules(sentence, vsid):
    keepGAP(sentence, vsid, True)


def word2VN(word, keep, unspecified):
    # remove specifications
    newspecs = {}
    newspecs["virtual"] = CdaSpec("tag", "virtual", "true", 0)

    for k, v in word.specs.items():
    #print "%s [%s]: %s (%d)" % (k, v.type, v.value, v.pointer)
        if v.type != 'tag':
            newspecs[k] = v
        elif k in keep:
            if k in unspecified:
                newspecs[k] = CdaSpec("tag", k, unspecified[k], 0)
            else:
                newspecs[k] = v
        # coarsen tags for virtual nodes
        if k == 'cat':
            newspecs[k] = CdaSpec("tag", k, vn_tag_converter(v.value), 0)
    word.specs = newspecs
    word.word = "[virtual]"


# nodes attached with this label are retained if their head is retained
predictables = ['SBJ']
# as above, but only if their head is not virtual
cond_predict = ['PMOD', 'ROOT', "VC", "CONJ", "IM", "PRD", "SUB",
                "OPRD", "PRT", "LGS", "LOC-PRD", "EXTR", "DTV", "PUT",
                "PRD-PRP", "PRD-TMP", "LOC-OPRD"]

my_comment = '''

old--coord-->and--conj-->chairman

 122723 NMOD     -
  50791 P        -
  43791 PMOD     c
  33704 SBJ      p
  25903 OBJ      c
  18318 ROOT     c
  17803 ADV      -
  13679 VC       c -> c und - ausprobieren
  12357 COORD    -
  11592 NAME     -
  10222 DEP      -
   9887 TMP      -
   9738 CONJ     c
   6951 LOC      -
   6742 APPO     -
   6707 AMOD     -
   6181 IM       c?(OPRD-IM 
   6017 PRD      c?
   5062 SUB      c? (whether something)
   4440 OPRD     c(?)
   4017 SUFFIX   -
   2607 TITLE    -
   1927 MNR      ??(Herausfinden, was das genau ist. eher nihct) treat--> like, XXX have been gaining circulation (gaining-MNR->without)
   1919 DIR      -
   1722 POSTHON  -
   1530 PRP      - xxx-PRP-> because (explanation)
   1174 PRT      c(?) turned->down, show->up
   1169 LGS      c? ordered->by, proposed->by etc.
    990 PRN      -
    635 EXT      -
    271 LOC-PRD  c
    257 EXTR     c? verbindung extr - prd  --- dependent variiert stark
    184 DTV      c (give->to etc) dependent immer to?
    107 PUT      c? regent ist put


GAP generell:
nur vorhersagbar mit "and" (oder anderer COORD)
only the rightmost gap can be predicted
DEP-GAP ist auch GAP

     42 GAP-OBJ  c (aber nur eins?)
     36 GAP-SBJ  c
     32 GAP-TMP  c
     28 DEP-GAP
     24 BNF      -
     19 GAP-PRD
     18 GAP-LOC
     18 DIR-GAP
     15 PRD-PRP  c
     13 PRD-TMP  c
     12 LOC-OPRD c(?)
     11 VOC      -
     11 GAP-LGS
     10 GAP-PMOD
      9 ADV-GAP
      8 GAP-VC
      8 EXT-GAP
      5 GAP-NMOD
      3 DIR-PRD   c
      2 LOC-TMP   ?? (noch überprüfen)
      1 MNR-TMP   ??
      1 GAP-PUT
      1 GAP-PRP
      1 GAP-LOC-PRD
      1 EXTR-GAP
      1 AMOD-GAP
'''


def vn_tag_converter(tag):
    if tag in ["NN", "NNS", "NNP", "NNPS", "PRP", "$", "CD", "JJ", "IN", "RP", "TO"]:
        # JJ is not a noun but words with this tag behave just as nouns
        # he is uncertain/JJ  <--> he is the president/NN
        # same with IN:
        # she questiones wheter/IN it will work
        # she questioned the solution/NN
        # they showed up <--> they showed the mask
        return "NVIRT"
    if tag in ["VBD", "VBP", "VBZ", "VB", "MD", "VBN"]:
        #  VB woulh sometines be appropriate for this ("Do it") but
        #  usually isn't
        #  VBN does also not completely fit in
        return "VVIRT"
    return tag


def cpos(tag):
    ''' There are no coarse PoS tags for English'''
    return tag


def pad_with_unused_vns(s):
    '''adds unused VNs to the sentence s'''
    specs = {}
    specs['cat'] = CdaSpec('tag', "cat", "ZZZNOWORD", 0)
    specs['SYN'] = CdaSpec('dep', "SYN", "UNUSED", 0)
    specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
    new_word = CdaWord("[unused]", len(s), len(s)+1, specs)
    unused_id = len(s)+1
    s.append(new_word)
    # Add a virtual verb if there is none
    if len([x for x in s if t.isVirtualByFeat(x) and
            t.checkFeat(x, "cat", "VVIRT")]) == 0:
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "VVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
    # Add up to two virtual nouns
    num_NN_vns = len([x for x in s if t.isVirtualByFeat(x) and
                      t.checkFeat(x, "cat", "NVIRT")])
    for i in range(num_NN_vns, 1):
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "NVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
