#! /usr/bin/env python
# -*- coding:utf-8 -*-
# prints the sentence of a .cda file
# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>
# GPLv3 or later

from cda_parse.cdgoutparse import parser

import sys

from getopt import getopt

opts, args = getopt(sys.argv[1:], 'rhlt')

if ('-h','') in opts:
    print """usage: getsentence.py [opts] cda-files
opts:
   -h : help
   -r : print only real words (not virtual)
   -l : print line by line (e.g. as input for a tagger)
   -t : print tags (only with -l)
"""
    exit()

if ('-r','') in opts:
    filterfunc = lambda x: not x.specs.has_key("virtual")
else:
    filterfunc = lambda x: True

lbl = ('-l','') in opts
tags = ('-t','') in opts

for f in args:
    s = parser.parse(unicode(open(f).read(), 'utf-8'))
    s = filter(filterfunc, s)
    if lbl:
        if tags:
            sentence = "\n".join([w.word+"\t"+w.specs["cat"].value for w in s])
        else:
            sentence = "\n".join([w.word for w in s])
        print
    else:
        sentence = " ".join(["'"+w.word.replace("'",r"\'")+"'" for w in s])
    print sentence.encode('latin-1')
