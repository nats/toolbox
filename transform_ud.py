# -*- coding:utf-8 -*-
# Copyright (C) 2012-2013  Niels Beuck
# Copyright (C) 2013  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''rules for virtualizing UD'''


import tree as t
from cda_parse.cdgoutparse import CdaSpec, CdaWord


def keepGAP(sentence, vsid, onlyOne):
    '''gapping constructs can either be predicted completely or only the
       rightmost one.'''
    for w in sentence[:vsid]:
        # find the rightmost GAP child of a COORD
        if t.getLabel(w, "SYN") == "COORD":
            for gap in t.getChildrenOf(w, sentence, "SYN"):
                if "GAP" in t.getLabel(gap, "SYN"):
                    t.setNeeded(gap, True)
                    break


def applyAllRules(sentence, vsid):
    # keepGAP(sentence, vsid, True)
    pass

def word2VN(word, keep, unspecified):
    # remove specifications
    newspecs = {}
    newspecs["virtual"] = CdaSpec("tag", "virtual", "true", 0)

    for k, v in word.specs.items():
    #print "%s [%s]: %s (%d)" % (k, v.type, v.value, v.pointer)
        if v.type != 'tag':
            newspecs[k] = v
        elif k in keep:
            if k in unspecified:
                newspecs[k] = CdaSpec("tag", k, unspecified[k], 0)
            else:
                newspecs[k] = v
        # coarsen tags for virtual nodes
        if k == 'cat':
            newspecs[k] = CdaSpec("tag", k, vn_tag_converter(v.value), 0)
    word.specs = newspecs
    word.word = "[virtual]"


# nodes attached with this label are retained if their head is retained
predictables = ['nsubj', 'nsubj:pass', "csubj", "csubj:pass", "root",]
# as above, but only if their head is not virtual
cond_predict = ["obj", "aux", "aux:pass","ccomp", "xcomp", "compound:prt","cop", "expl","expl:pv", "expl:pass", "expl:impers", "iobj", "obl", "obl:npmod", "obl:tmod",  "orphan", "fixed", "case", "goeswith"]

# goeswith is not really clear: the right part is often needed as the
# left one wouldn't fit grammatically alone but predicting that still
# seems like a long shot.

head_always_right = ["cc", "compound",  "det",  "det:predet", "mark", "cc:preconj", ]


non_predictable = head_always_right + ["acl", "acl:relcl", "advcl", "advmod", "amod", "appos", "conj", "dep","discourse", "dislocated", "nmod", "nmod:gmod", "nmod:npmod", "nmod:poss", "nmod:tmod",  "nummod", "parataxis", "flat", "reparandum",  "vocative", "punct", "flat:name"]

punctuation_labels = ["punct"]

# TODO!
def vn_tag_converter(tag):
    return "NVIRT"

def cpos(tag):
    ''' There are no coarse PoS tags for English'''
    return tag

def pad_with_unused_vns(s):
    '''adds unused VNs to the sentence s'''
    specs = {}
    specs['cat'] = CdaSpec('tag', "cat", "ZZZNOWORD", 0)
    specs['SYN'] = CdaSpec('dep', "SYN", "UNUSED", 0)
    specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
    new_word = CdaWord("[unused]", len(s), len(s)+1, specs)
    unused_id = len(s)+1
    s.append(new_word)
    # Add a virtual verb if there is none
    if len([x for x in s if t.isVirtualByFeat(x) and
            t.checkFeat(x, "cat", "VVIRT")]) == 0:
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "VVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
    # Add up to two virtual nouns
    num_NN_vns = len([x for x in s if t.isVirtualByFeat(x) and
                      t.checkFeat(x, "cat", "NVIRT")])
    for i in range(num_NN_vns, 1):
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "NVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
