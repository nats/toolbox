# -*- coding:utf-8 -*-
# An evaluator for cda files without good documentation
# Copyright (C) 2012-2013  Niels Beuck
# Copyright (C) 2013  Arne Köhn <arne@arne-koehn.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''rules for virtualizing German'''


import tree as t
from cda_parse.cdgoutparse import CdaSpec, CdaWord


def objpToPP(sentence, vsid):
    for w in sentence:
        if t.getHead(w, "SYN") > vsid and t.checkLabel(w, "SYN", "OBJP"):
            #print "! objp replacement"
            t.setLabel(w, "SYN", "PP")


def detrunc(sentence, vsid):
    for w in sentence:
        if t.isVirtual(w, vsid) and t.checkFeat(w, 'cat', 'TRUNC'):
            cs = t.getChildrenOf(w, sentence, "SYN")
            cat = None
            for c in cs:
                if t.checkLabel(c, "SYN", "KON"):
                    cs2 = t.getChildrenOf(c, sentence, "SYN")
                    for c2 in cs2:
                        if t.checkLabel(c2, "SYN", "CJ"):
                            cat = t.getFeat(c2, "cat")
                            break
                if cat:
                    break
            if cat:
                t.setFeat(w, "cat", cat)


def foldKonChain(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.isVirtual(w, vsid) \
           and t.isVirtual(head, vsid) \
           and t.checkLabel(w, "SYN", "KON") \
           and (t.checkLabel(head, "SYN", "KON")
                or t.checkLabel(head, "SYN", "APP"))\
           and not(t.isNeeded(head)):
            #print "! kon chain folding"
            t.foldUp(w, sentence)


def foldAppChain(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.isVirtual(w, vsid) \
           and t.isVirtual(head, vsid) \
           and t.checkLabel(w, "SYN", "APP") \
           and (t.checkLabel(head, "SYN", "KON")
                or t.checkLabel(head, "SYN", "APP")) \
           and not(t.isNeeded(head)):
            #print "! app chain folding"
            t.foldDown(w, sentence)


def foldAuxChain(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.isVirtual(w, vsid) \
           and t.isVirtual(head, vsid) \
           and t.checkLabel(w, "SYN", "AUX"):
            downCat = t.getFeat(w,    "cat")
            upCat = t.getFeat(head, "cat")
            zombieCat = downCat[0:2]+upCat[2:]
            #print "! aux chain folding"
            t.foldDown(w, sentence)
            t.setFeat(head, 'cat', zombieCat)
            #valenzen werden später gesetzt


def keepKons(sentence, vsid):
    '''
    a kon dependent is kept, if:
    - it is virtual (otherwise it is kept anyway)
    - the label is kon (that's what we said above)
    - the head label is also a kon (so, we have a kon chain)
    - the head is needed  (either in-prefix)
    '''
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.isVirtual(w, vsid) \
           and t.checkLabel(w, "SYN", "KON") \
           and t.checkLabel(head, "SYN", "KON") \
           and t.isNeeded(head):
            #print "! keeping a kon"
            t.setNeeded(w, True)


def keepCJs(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.isVirtual(w, vsid) \
           and t.checkLabel(w, "SYN", "CJ") \
           and t.checkLabel(head, "SYN", "KON") \
           and t.isNeeded(head):
            #print "! keeping a cj"
            t.setNeeded(w, True)


def keepTruncs(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.checkFeat(head, 'cat', 'TRUNC') and \
           not(t.isVirtual(head, vsid)) and \
           t.checkLabel(w, "SYN", "KON"):
            t.setNeeded(w, True)


def keepPseudoKon(sentence, vsid):
    for w in sentence:
        head = t.getHeadOf(w, sentence, "SYN")
        if not head:
            continue
        if t.checkFeat(w, 'cat2', 'KON') and \
           t.checkLabel(w, "SYN", "ADV") and \
           t.checkLabel(head, "SYN", "NEB") and \
           t.isNeeded(head):
            #print "! keeping a pseudokon"
            t.setNeeded(w, True)


def applyAllRules(sentence, vsid):
    objpToPP(sentence, vsid)
    # pred transformaion
    detrunc(sentence, vsid)
    foldKonChain(sentence, vsid)
    foldAppChain(sentence, vsid)
    foldAuxChain(sentence, vsid)
    #print "intermediate:"
    #printsen(sentence)
    keepKons(sentence, vsid)
    keepTruncs(sentence, vsid)
    keepCJs(sentence, vsid)
    keepPseudoKon(sentence, vsid)


def applyTransform(sentence, vsid):
    objpToPP(sentence, vsid)
    # pred transformaion
    detrunc(sentence, vsid)
    foldKonChain(sentence, vsid)
    foldAppChain(sentence, vsid)


def applyFolding(sentence, vsid):
    foldAuxChain(sentence, vsid)


def applyImplicit(sentence, vsid):
    keepKons(sentence, vsid)
    keepTruncs(sentence, vsid)
    keepCJs(sentence, vsid)
    keepPseudoKon(sentence, vsid)


def word2VN(word, keep, unspecified):
    # remove specifications
    newspecs = {}
    newspecs["virtual"] = CdaSpec("tag", "virtual", "true", 0)
    newspecs["base"] = CdaSpec("tag", "base", "unknown", 0)

    # hack, because, person and other features are missing in the gold
    # annotation
    newspecs["person"] = CdaSpec("tag", "person", "bot", 0)
    newspecs["number"] = CdaSpec("tag", "number", "bot", 0)
    newspecs["case"] = CdaSpec("tag", "case", "bot", 0)

    for k, v in word.specs.items():
    #print "%s [%s]: %s (%d)" % (k, v.type, v.value, v.pointer)
        if v.type != 'tag':
            newspecs[k] = v
        elif k in keep:
            if k in unspecified:
                newspecs[k] = CdaSpec("tag", k, unspecified[k], 0)
            else:
                newspecs[k] = v

        if k == 'cat' and (v.value[0:2] == 'VV'):
            newspecs['valence'] = CdaSpec("tag", "valence", "acis?+d?", 0)
        if k == 'cat' and (v.value[0:2] == 'VM'):
            newspecs['valence'] = CdaSpec("tag", "valence", "ax", 0)
        if k == 'cat' and (v.value[0:2] == 'VA'):
            newspecs['valence'] = CdaSpec("tag", "valence", "egx", 0)
            newspecs['avz'] = CdaSpec("tag", "avz", "allowed", 0)
            newspecs['extravalence'] = CdaSpec("tag", "extravalence",
                                               "daci", 0)
            try:
                newspecs['base'] = word.specs['base']
            except:
                pass
        # coarsen tags for virtual nodes
        if k == 'cat':
            newspecs[k] = CdaSpec("tag", k, vn_tag_converter(v.value), 0)
    word.specs = newspecs
    word.word = "[virtual]"


# nodes attached with this label are retained if their head is retained
predictables = ['SUBJ', 'SUBJC', 'PN', 'CJ']
# as above, but only if their head is not virtual
cond_predict = ['OBJA', 'OBJD', 'OBJC', 'PRED', 'OBJP', 'AUX', 'PART', 'S']


def vn_tag_converter(tag):
    '''converts NE and NN to NVIRT and VVFIN and VAFIN to VVIRT
    all else is untouched'''
    if tag[0] == "N":
        return "NVIRT"
    if tag == "VVFIN" or tag == "VAFIN":
        return "VVIRT"
    return tag


def cpos(tag):
    if tag in ["NE", "NN"]:
        return "N"
    if tag in ["VVFIN", "VVINF", "VVPP", "VVIZU", "VVIMP",
               "VAFIN", "VAINF", "VAPP", "VAIMP", "VMFIN",
               "VMINF", "VMPP"]:
        return "V"
    if tag in ["APPR", "APPRART", "APPO"]:
        return "PREP"
    if tag in ["PPOSAT", "ART", "PWAT", "PRELAT", "PPOSAT", "PIAT",
               "PIDAT", "PDAT"]:
        return "ART"
    if tag in ["PWS", "PRF", "PRELS", "PPOSS", "PPER", "PIS", "PDS"]:
        return "PRO"
    if tag in ["ADV", "ADJD"]:
        return "ADV"
    else:
        return tag

def pad_with_unused_vns(s):
    '''adds unused VNs to the sentence s'''
    specs = {}
    specs['cat'] = CdaSpec('tag', "cat", "ZZZNOWORD", 0)
    specs['SYN'] = CdaSpec('dep', "SYN", "UNUSED", 0)
    specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
    new_word = CdaWord("[unused]", len(s), len(s)+1, specs)
    unused_id = len(s)+1
    s.append(new_word)
    # Add a virtual verb if there is none
    if len([x for x in s if t.isVirtualByFeat(x) and
            t.checkFeat(x, "cat", "VVIRT")]) == 0:
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "VVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
    # Add up to two virtual nouns
    num_NN_vns = len([x for x in s if t.isVirtualByFeat(x) and
                      t.checkFeat(x, "cat", "NVIRT")])
    for i in range(num_NN_vns, 2):
        specs = {}
        specs['cat'] = CdaSpec('tag', "cat", "NVIRT", 0)
        specs['SYN'] = CdaSpec('dep', "SYN", "", unused_id)
        specs['virtual'] = CdaSpec('tag', "virtual", "true", 0)
        new_word = CdaWord("[virtual]", len(s), len(s)+1, specs)
        s.append(new_word)
