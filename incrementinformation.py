#! /usr/bin/env python                                                                                                                         
# -*- coding: utf-8 -*-                                                                                                                        

# prints information about each increment in a sentence.

# Copyright (C) 2010  Arne Köhn <arne@arne-koehn.de>                                                                                           
# GPLv3 or later                                                                                                                               

import sys, os
from cda_parse import cdgoutparse
from cda_parse.cdgoutparse import parser

import sys

def doit(sentence):
    position = 1
    openwords = set()
    numowold = 0 # number of words that pointed to the future in the last run
    temp = set()
    for word in sentence:
        for ow in openwords:
            if ow.specs["SYN"].pointer == position:
                temp.add(ow)
        openwords.difference_update(temp)
        temp.clear()
        if word.specs["SYN"].pointer > position:
            openwords.add(word)

        print word.word.encode('latin-1'), # len(openwords),
        print len(openwords)
        position += 1
        numowold = len(openwords)

if __name__ == '__main__':
    for arg in sys.argv[1:]:
        sentence = parser.parse(unicode(open(arg).read(), 'latin-1'))
        doit(sentence)
        print
