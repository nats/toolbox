#! /bin/sh
# Author Arne Köhn <arne@arne-koehn.de>
# this creates a cdg script that uses anno2parse to parse
# all given sentence structures and prints the constraint violations
# public domain (do what you want to do with it)
cat <<EOF
set info off
set warning off
load grammar/deutsch/deutsch.cdg
set tokenizer none
set ignorethreshold 0.951
weight 'NONSPEC-Kante (SYN)' 0.5
set usenonspec on
set timelimit 300000
set maxsteps 22
set freezing-level off
set incrAutoFinalize off
set incrOptimizeAnalysis off
set useVirtualNodes on
set matchVirtualNodes on
set taggerCommand hunpos-incr.sh
uselevel REF
EOF

for i in `echo $@ | python negra_filename_sorter.py`; do
    if [ -f $i ]; then
	bname=`basename $i | sed "s/\..*//"`
        cat <<EOF
load $i
anno2parse $bname
printparse $bname
EOF
    fi
done
echo "quit"
